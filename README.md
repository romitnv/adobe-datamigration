This file will contains details for the Migration scripts for Adobe Data Migration Project


Below is the step to compile and build the jar file for Hortonworks platform

javac -cp .:/usr/hdp/2.3.2.0-2950/hadoop/client/hadoop-common-2.7.1.2.3.2.0-2950.jar MigrationValidation.java

jar cvf MigrationValidation.jar MigrationValidation.class

Below are the steps to buid the jar on Cloudera Platform

javac -cp .:/usr/lib/hadoop/client-0.20/hadoop-common-2.6.0-cdh5.5.0.jar MigrationValidation.java

jar cvf MigrationValidation.jar MigrationValidation.class


Following is the command to run the jar file

yarn jar MigrationValidation.jar MigrationValidation -i SampleInput.txt -o SampleOutput.txt

The sample of input and output files are provided as part of the repo.
