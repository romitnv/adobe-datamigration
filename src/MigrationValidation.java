import java.io.*;
import java.util.*;
import java.io.IOException;
import java.util.logging.*;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileChecksum;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocalFileSystem;
import org.apache.hadoop.fs.Path;

public class MigrationValidation
{
    static final Logger logger = Logger.getLogger("MigrationValidation");  
    public static void main(String args[])
    {
        try
        {
        FileHandler lfh = new FileHandler("./DataMigration.log");
        logger.addHandler(lfh);
        logger.setUseParentHandlers(false);
        SimpleFormatter formatter = new SimpleFormatter();  
        lfh.setFormatter(formatter);
        boolean status = false;
        if(args.length != 4)
        {
            printHelp();
        }
        StringBuffer inputFile = new StringBuffer();
        StringBuffer outputFile = new StringBuffer();
        status = validateArgs(args,inputFile,outputFile);
        if (status == false || inputFile == null || outputFile == null)
        {
            printHelp();
        }
        generateCheckSum(inputFile.toString(), outputFile.toString());
       }
       catch (IOException e)
       {
           e.printStackTrace();
       }
        
    }

    public static void printHelp()
    {
        System.out.println("Usage: MigrationValidation -i <inputFile> -o <outputFile>");
        System.exit(0);
    } 
    public static boolean isAsciiPrintable(String str) {
    if (str == null) {
         return false;
     }
    int sz = str.length();
    for (int i = 0; i < sz; i++) {
        if (isAsciiPrintable(str.charAt(i)) == false) {
            return false;
        }
    }
    return true;
  } 
  public static boolean isAsciiPrintable(char ch) {
      return ch >= 32 && ch < 127;
  }

public static void generateCheckSum(String inpFileName, String outFileName)
{
        String filePath = null;
     try
        {
        Configuration conf = new Configuration();
        FileSystem hadoopFS = FileSystem.get(conf);
        File inputFile = new File(inpFileName);
        Scanner sc = new Scanner(inputFile);
        PrintWriter outputFile = new PrintWriter(new FileWriter(new File(outFileName),true));
        while(sc.hasNextLine()) {
            filePath = sc.nextLine();
            if (!(filePath.equals("")))
            {
                FileChecksum hdfsChecksum = hadoopFS.getFileChecksum(new Path(filePath));
                String checksum[] = (hdfsChecksum.toString()).split(":");
                if(!isAsciiPrintable(checksum[1]))
                {
                    logger.info("The value of file path is not printable" + filePath);
                }
                else
                {
                    outputFile.printf("%s,%s\n", filePath,checksum[1]);
                }
            }   
     }
        outputFile.close();
    }
    catch(FileNotFoundException e) {
        logger.info("Issue opening the input file " + filePath); 

    }
    catch(IOException e) {
        logger.info("Issue getting checksum for file " + filePath);
    }
}

    public static boolean validateArgs(String myArgs[],StringBuffer inputFile, StringBuffer outputFile)
    {
        @SuppressWarnings("unchecked") 
        Map<String,String> argmap = new HashMap();
        argmap.put("-i", "inputFile");
        argmap.put("-o", "outputFile");
        for (int i=0; i < myArgs.length; i++)
        {
           String checkArg = myArgs[i].toLowerCase();
           if (checkArg.startsWith("-"))
           {
               String val = argmap.get(checkArg);
               if (val == null)
               {
                   printHelp();
               }
               else if (val.equals("inputFile"))
               {
                   inputFile.replace(0,inputFile.length(),myArgs[i+1]);
               }
               else if (val.equals("outputFile"))
               {
                   outputFile.replace(0,outputFile.length(),myArgs[i+1]);

               }
            
           }

        }
         return true;
    }

}

